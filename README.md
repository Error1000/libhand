# libhand
  
A crappy little tool i made to handle libraries for c++ makefiles.  
  
  
------------------------Example rules---------------------  
  
~A{  
LDFLAGS; add-somethingA | something-elseA  
}  
  
~B{  
INC: b  
LIB: b  
}
  
~C{  
INC: c  
LIB: c  
DEPENDS_ON: B  
}  
  
~D{  
INC: d  
LIB: d  
RESINC; somethingD  
}  
  
---------------------------------------------------------  
  
Will do the following:  
reorder the libraries depending on the DEPENDS_ON flags, so it reorders them to process them in this order:  
A,  
C,  
B,  
D  

Processes each:  
For A:  
- Add -add-somethingA and -something-elseA to the current flags of LDFLAGS  
  
For C:  
- If this is the first time we process INC, then replace what is there with -Ic else add to it -Ic  
- If this is the first time we process LIB, then replace what is there with -lc else add to it -lc  
  
For B:  
- If this is the first time we process INC, then replace what is there with -Ib else add to it -Ib  
- If this is the first time we process LIB, then replace what is there with -lb else add to it -lb  
  
For D:  
- If this is the first time we process INC, then replace what is there with -Id else add to it -Id  
- If this is the first time we process LIB, then replace what is there with -ld else add to it -ld  
- Add to the curretn arguments of RESINC -somethingD  
  
  
So if we have a makefile like this:  
INC = -something  
LIB = -something-else  
LDFLAGS = -something -from-makefile  
RESINC = -something-from-makefile  
  
It will produce:  
  
INC = -Ic -Ib -Id  
LIB = -lc -lb -ld  
LDFLAGS = -someting -from-makefile -add-soemthingA -something-elseA  
RESING = -something-from-makefile -somethingA  

