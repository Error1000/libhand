import os
import sys
import re

DEBUG = False

successExitcode = 0
generalErrorExitcode = 1
interpretationErrorExitcode = 16

def printUsageAndExit():
    print("Usage: python main.py --rules rulesFile --topatch toPatchFile")
    exit(successExitcode)

#FIXME: This uses os.popen which is probably unsafe, not to mention that when the program uses this it might expect something to happen but technically the command could do anything
def run(cmd):
    try:
        cmd = str(cmd)
    except ValueError:
        print("The command to run: " + cmd + " could not be transformed to a string!")
        exit(generalErrorExitcode)
    outputOfCommand = os.popen(cmd).read()

    try:
        outputOfCommand = str(outputOfCommand)
    except ValueError:
        print("The output of the command could not be transformed to a string!")
        exit(generalErrorExitcode)
    return outputOfCommand



def fileExists(filePath):
    outputOfIsFile = os.path.isfile(filePath)

    try:
        outputOfIsFile = bool(outputOfIsFile)
    except ValueError:
        print("The output of the os.path.isfile function could not be transformed to a boolean!")
        exit(1)
    return outputOfIsFile


def concatAll(arr):
    concated = ""
    for lin in arr: concated += lin
    return concated

argvSiz = len(sys.argv)

#This is to test if we don't have 2 "arguments"
if argvSiz - 1 < 2*2: printUsageAndExit()

rulesPath = ""
toPatchPath = ""

############################################IMPUT PARSER############################################
#Starts at 1 because the first arg is the program's name

i = 1

while True:
    currentArg = sys.argv[i]

    try:
        currentArg = str(currentArg)
    except ValueError:
        print("An argument passed to the program could not be transformed to a string!")
        exit(generalErrorExitcode)

    currentArg = currentArg.strip().replace("\n","")

    if currentArg == "--rules":
        if i < argvSiz - 1:
            i += 1
            try:
                rulesPath = str(sys.argv[i])
            except ValueError:
                print("The rules argument could not be transformed to a string!")
                exit(generalErrorExitcode)
        else:
            printUsageAndExit()

    elif currentArg == "--topatch":
        if i < argvSiz - 1:
            i += 1
            try:
                toPatchPath = str(sys.argv[i])
            except ValueError:
                print("The patch argument could not be transformed to a string!")
                exit(generalErrorExitcode)
        else:
            printUsageAndExit()

    else:
        printUsageAndExit()

    i += 1
    if i > argvSiz-1: break


if not fileExists(rulesPath):
    print("File: " + rulesPath + " doesn't exist!")
    exit(generalErrorExitcode)

if not fileExists(toPatchPath):
    print("File: " + toPatchPath + " doesn't exist!")
    exit(generalErrorExitcode)

with open(rulesPath, "r") as rulesFile:
    rules = rulesFile.read().split("\n")


with open(toPatchPath, "r") as toPatchFile:
    toPatchData = toPatchFile.read().split("\n")

for i in range(len(toPatchData)):
    toPatchData[i] = toPatchData[i] + "\n"

####################################################################################################




############################################TOKENIZER############################################
if DEBUG: print("Running tokenizer!")
libName = ""
inBody = False
inLib = False
currentLib = []
currentBody = []
libs = []

for line in rules:
    readingName = False
    bodyLine = ""
    for ch in line:
        if ch == "#":
            break

        if ch == "}": # body end
            if bodyLine != "": currentBody.append(bodyLine)

            if inLib:
             currentLib.append(currentBody)
             libs.append(currentLib)
             inLib = False
             currentLib = []


            inBody = False
            currentBody = []


        if inBody:
            bodyLine += ch

        if ch == "{": # body start
            readingName = False

            if inLib:
             currentLib.append(libName)

            inBody = True

        if readingName:
            libName += ch

        #Body starters
        if ch == "~":
            if inBody and inLib:
                print("Missing '}'(body close) for body of library: " + libName + ", at line " + str(rules.index(line)-1) )
                exit(interpretationErrorExitcode)
            elif inBody and not inLib:
                print("Missing '}'(body close) for unkown body at line " + str(rules.index(line)-1) )
                exit(interpretationErrorExitcode)
            libName = ""
            inLib = True
            readingName = True



    if inBody and bodyLine != "":
        currentBody.append(bodyLine)

if DEBUG:
    print("Finished running tokenizer!")
    print()
#################################################################################################



if DEBUG:
    print("Current order of libraries: ")
    for library in libs:
        print(library[0])

    print()



############################################DEPENDENCY AUTOMATER############################################
if DEBUG: print("Running dependency automater!")

backUpLibs = libs

for i in range(len(backUpLibs)):
    if DEBUG:
        print()
        print()

    lib = backUpLibs[i]
    if lib[0] == "":
        print("A library from the list has no name!")
        exit(interpretationErrorExitcode)
    if DEBUG: print("Evaluating library: " + lib[0])
    nrOfDependsOnStatements = 0

    for statementI in range(len(lib[1])):
        statement = str(lib[1][statementI])
        if not statement.__contains__(":") and not statement.__contains__(";"):
            print("Tokenizer found what it thought was a statement but actually can't be because it doesn't contain the delimiter(':' or ';') in library: " + lib[0] + "!")
            exit(interpretationErrorExitcode)

        splitStatement = statement.split(":")
        for k in range(len(splitStatement)): splitStatement[k] = splitStatement[k].strip()

        if splitStatement[0] == "DEPENDS_ON":
            nrOfDependsOnStatements += 1
            dependsLibs = splitStatement[1].strip().split("|")

            for v in range(len(dependsLibs)):
                dependsLibs[v] = dependsLibs[v].strip()

            for dep in dependsLibs:
                if DEBUG: print("Found dependency: " + dep + ", of library: " + lib[0] + ", evaluating it ...")

                if dep == "":
                    print("A dependency of the library: " + lib[0] + ", has no name!!!")
                    exit(interpretationErrorExitcode)

                libLocationInPossiblyMixedVector = libs.index(lib)
                depLocationInPossiblyMixedVector = -1
                for lI in range(len(libs)):
                    if libs[lI][0] == dep:
                        depLocationInPossiblyMixedVector = lI
                        break

                if depLocationInPossiblyMixedVector == -1:
                    print("Couldn't find dependency: " + dep + ", of library: " + lib[0] + ", in the list of libraries!!!")
                    exit(interpretationErrorExitcode)

                if depLocationInPossiblyMixedVector < libLocationInPossiblyMixedVector: # if dep is in front of library that depends on it
                    #Switch positions
                    tempLib = libs[depLocationInPossiblyMixedVector]
                    libs[depLocationInPossiblyMixedVector] = libs[libLocationInPossiblyMixedVector]
                    libs[libLocationInPossiblyMixedVector] = tempLib
                if DEBUG: print("Successfully evaluated dependency: " + dep + ", of library: " + lib[0] + "!")

        if nrOfDependsOnStatements > 1:
            print("Number of DEPENDS_ON statements of library: " + lib[0] + ", is grater than 1, this can lead to problems with the dependency automater, just repeating the same statement multiple times is a bad idea!")
            print("This is not fatal so the dependency automater will continue, but be warned!")

    if DEBUG:
        print("Successfully evaluated library: " + lib[0] + "!")

if DEBUG:
    print("Finished running dependency automater!")
    print()
############################################################################################################


if DEBUG:
    print("Current order of libraries: ")
    for library in libs:
        print(library[0])

    print()

############################################INTERPRETER############################################
if DEBUG: print("Running interpreter!")

append = False

seenVars = []

#We go through each lib in the order it is in the array and apply it's changes to the toPatch file
for i in range(len(libs)):
    for statementI in range(len(libs[i][1])):
        statement = str(libs[i][1][statementI])
        append = not statement.__contains__(":")

        #Establish what to search for and the flags
        splitStatement = statement.split(":")
        if len(splitStatement) != 2: splitStatement = statement.split(":")
        toSearchFor = splitStatement[0].strip()
        flags = splitStatement[1].strip().split("|")
        for flagI in range(len(flags)): flags[flagI] = flags[flagI].strip()


        #"Edge cases"
        #Ignore teh DEPENDS_ON statement
        if splitStatement[0] == "DEPENDS_ON": continue

        #Format the string
        toSearchFor.replace("=", "")

        #Find line
        out = str(run("echo '" + concatAll(toPatchData) + "' | grep -nP '^" + toSearchFor + " *=.*$' 2> /dev/null"))

        #Parse grep output
        splitOut = out.split(":")
        try:
         lineNumber = int(str(splitOut[0])) - 1
        except ValueError:
         print("Failed to parse output fo the command used to find the statement: " + toSearchFor + "!")
         exit(generalErrorExitcode)

        #Split strings
        variableSplit = toPatchData[lineNumber].replace("\n", "").split("=")

        if len(variableSplit) < 2:
            print("Searching for: " + toSearchFor + ", yielded line: " + toPatchData[lineNumber] + ", of toPatch file, which is not a bash variable declaration!")
            exit(interpretationErrorExitcode)

        if len(variableSplit) > 2:
            #If we split in more than two parts concatenate them back
            s = len(variableSplit)
            for l in range(s):
                variableSplit[1] += "=" + variableSplit[l]
                del variableSplit[l]


        variableName = variableSplit[0]
        toEditArgs = ""
        shVars = ""

        spaceSplitVar = variableSplit[1].split(" ")
        allowedShVars = True
        for thing in spaceSplitVar:
            if thing == "": continue
            if re.match(r"\$\(.*\)", thing):
                if allowedShVars:
                    shVars += thing + " "
                else:
                    print("Shell variables are only allowed to be referenced at the start of variables referenced by a statement, the variable that was violating this in the toPatch file is: " + variableSplit[0] + "!")
                    exit(interpretationErrorExitcode)
            else:
                allowedShVars = False
                toEditArgs += thing + " "

        variableName = variableName.strip()


        #S.C.U.M.M ( smart context-dependant useful multi-variable matcher )
        #WARNING: This is assuming that in the makefile the used variables INC, LIBDIR, and LIB, represent the right compiler arguments and other stuff like that
        category = ""
        if variableName.__contains__("INC"):
            category = "I"
        elif variableName.__contains__("LIBDIR"):
            category = "L"
        elif variableName.__contains__("LIB"):
            category = "l"

        #Perform operation
        if not append:
            s = False
            for var in seenVars:
                if variableName == var:
                    s = True
                    break

            if not s:
                seenVars.append(variableName)
                toEditArgs = ""

        for flag in flags:
            if flag != "":
                toEditArgs += " -" + category + flag

        toEditArgs = toEditArgs.strip()
        variableName = variableName.strip()

        if DEBUG: print("Current line is: " + toPatchData[lineNumber].replace("\n", "").strip() + " !")
        toPatchData[lineNumber] = variableName + " = " + shVars + " " + toEditArgs + "\n"
        if DEBUG:
            print("That line was changed to: " + toPatchData[lineNumber].replace("\n", "").strip() + " !")
            print()

if DEBUG:
    print("Finished running interpreter!")
    print()
###################################################################################################

#Output result
print(concatAll(toPatchData).strip())

exit(successExitcode)





